import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router: Router) {}
  login(username: any): void {
    localStorage.setItem('username', username);
  }
  logout(): void {
    localStorage.removeItem('username');
    localStorage.removeItem('nameSearch');
    localStorage.removeItem('groupSearch');
    this.router.navigate(['/login']);
  }
  isLoggedIn(): boolean {
    return localStorage.getItem('username') !== null;
  }
  getUser(): any {
    return localStorage.getItem('username');
  }
  navigateBack(): void {
    this.router.navigate(['/employee']);
  }
}

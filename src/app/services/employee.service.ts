import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private datas: any[] = [];
  private sortDirection: 'asc' | 'desc' = 'asc';
  private currentSortKey: any = '';
  constructor() {
    // Generate 100 dummy data
    for (let i = 1; i <= 101; i++) {
      this.datas.push({
        id: i,
        username: `user${i}`,
        firstName: `First ${i}`,
        lastName: `Last ${i}`,
        email: `user${i}@example.com`,
        birthDate: this.randomDate(
          new Date(1970, 0, 1),
          new Date(2000, 11, 31)
        ),
        basicSalary: this.randomNumber(2000, 5000),
        status: i % 2 === 0 ? 'Active' : 'Inactive',
        group: i % 3 === 0 ? 'Group A' : 'Group B',
        description: `Description for user ${i}`,
      });
    }
  }
  // Method to generate a random date between two dates
  private randomDate(start: Date, end: Date): Date {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    );
  }

  toggleSort(): void {
    this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
  }

  private randomNumber(min: number, max: number): number {
    return Math.round(Math.random() * (max - min) + min);
  }
  getAllEmployees(searchName: string, searchGroup: string): any[] {
    const result = this.datas.filter(
      (employee) =>
        employee.firstName.toLowerCase().includes(searchName.toLowerCase()) &&
        employee.group.toLowerCase().includes(searchGroup.toLowerCase())
    );
    return result;
  }

  getEmployeesPage(
    page: number,
    pageSize: number,
    searchName: string,
    searchGroup: string
  ): any[] {
    const startIndex = (page - 1) * pageSize;
    const filteredEmployees = this.datas
      .filter(
        (employee) =>
          employee.firstName.toLowerCase().includes(searchName.toLowerCase()) &&
          employee.group.toLowerCase().includes(searchGroup.toLowerCase())
      )
      .slice(startIndex, startIndex + pageSize);

    return filteredEmployees;
  }
  sortEmployeesByKey(key: any): void {
    if (key === this.currentSortKey) {
      this.toggleSort();
    } else {
      this.currentSortKey = key;
      this.sortDirection = 'asc'; // Default to ascending when changing sort key
    }
    switch (key) {
      case 'id':
        this.datas.sort((a, b) => {
          if (this.sortDirection === 'asc') {
            return a[key] - b[key];
          } else {
            return b[key] - a[key];
          }
        });
        break;

      case 'group':
        this.datas.sort((a, b) => a.group.localeCompare(b.group));
        this.datas.sort((a, b) => {
          if (this.sortDirection === 'asc') {
            return a.group.localeCompare(b.group);
          } else {
            return b.group.localeCompare(a.group);
          }
        });
        break;
    }
  }
  getEmployeeById(id: number): any {
    return this.datas.find(employee => employee.id === id);
  } 
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private users: any[] = [
    {
      id: 1,
      username: 'superadmin',
      role : "superadmin",
      email: 'superadmin@example.com',
      password: 'superadmin',
    },
    {
      id: 2,
      username: 'operator',
      role: "operator",
      email: 'operator@example.com',
      password: 'operator',
    },
  ];
  constructor() {}
  getUsers(): any[] {
    return this.users;
  }
  getUserByUsername(username: any): any {
    return this.users.find(user => user.username === username);
  }
}

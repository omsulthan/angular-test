import { Component, OnInit } from '@angular/core';

import {
  AbstractControl,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ValidatorFn,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  users: any[] = [];
  nullError: boolean = false;
  wrongError: boolean = false;

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private userService: UserService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

    this.users = this.userService.getUsers();
  }

  async onSubmit(): Promise<void> {
    if (this.loginForm.valid) {
      const username = this.loginForm.value.username;
      const password = this.loginForm.value.password;

      const getUser = this.userService.getUserByUsername(username);

      if (getUser && getUser.password === password) {
        await this.authService.login(getUser.username);
        await this.router.navigate(['/employee']);
      } else {
        this.wrongError = true;
        this.nullError = false;
      }
    } else {
      // Error Form Kosong
      this.nullError = true;
      this.wrongError = false;
    }
  }
}

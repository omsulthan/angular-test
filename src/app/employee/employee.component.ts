import { Component, TemplateRef } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { EmployeeService } from '../services/employee.service';
import { ToastService } from '../services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent {
  currentPage: number = 1;
  pageSize: number = 10;
  nameSearch: string = '';
  groupSearch: string = '';
  employees: any = '';
  constructor(
    private auth: AuthService,
    private employee: EmployeeService,
    private toastService: ToastService,
    private router: Router
  ) {}
  user = this.auth.getUser();

  ngOnInit(): void {
    this.nameSearch = localStorage.getItem('nameSearch') ?? '';
    this.groupSearch = localStorage.getItem('groupSearch') ?? '';
    this.search();
  }

  loadEmployees(): void {
    this.employees = this.employee.getEmployeesPage(
      this.currentPage,
      this.pageSize,
      this.nameSearch,
      this.groupSearch
    );
    console.log(this.employees);
  }

  totalPages: any =
    this.employee.getAllEmployees(this.nameSearch, this.groupSearch).length /
    this.pageSize;

  sortBy(key: any): void {
    this.employee.sortEmployeesByKey(key);
    this.loadEmployees();
  }

  search(): void {
    this.loadEmployees();
    this.currentPage = 1;
    this.totalPages =
      this.employee.getAllEmployees(this.nameSearch, this.groupSearch).length /
      this.pageSize;
    localStorage.setItem('nameSearch', this.nameSearch);
    localStorage.setItem('groupSearch', this.groupSearch);
  }

  onPageChange(page: number): void {
    this.currentPage = page;
    console.log(this.pageSize);

    this.loadEmployees();
  }
  deleteEmployee(template: TemplateRef<any>): void {
    this.toastService.show({
      template,
      class: 'bg-red-500 text-white transition mb-5',
      delay: 1500,
    });
  }
  editEmployee(template: TemplateRef<any>): void {
    this.toastService.show({
      template,
      class: 'bg-yellow-500 text-white transition mb-5',
      delay: 1500,
    });
  }
  getDetail(id: any): void {
    this.router.navigate([`/employee-detail/${id}`]);
  }
  onPageSizeChange(event: any): void {
    this.pageSize = parseInt(event.target.value);
    this.currentPage = 1;
    this.loadEmployees();
    this.totalPages =
      this.employee.getAllEmployees(this.nameSearch, this.groupSearch).length /
      this.pageSize;
  }
  addPage(): void {
    this.router.navigate(['/add-employee']);
  }
}

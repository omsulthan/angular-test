import { Component, inject } from '@angular/core';
import { NgTemplateOutlet, CommonModule  } from '@angular/common';
import { NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from './services/toast.service';

@Component({
  selector: 'app-toasts',
  standalone: true,
  imports: [NgbToastModule, NgTemplateOutlet, CommonModule],
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts; trackBy: trackByFn"
      [class]="toast.class"
      [autohide]="true"
      [delay]="toast.delay || 1000"
      (hidden)="toastService.remove(toast)"
    >
      <ng-template [ngTemplateOutlet]="toast.template"></ng-template>
    </ngb-toast>
  `,
  host: {
    class: 'toast-container fixed top-0 end-0 p-3',
    style: 'z-index: 1200',
  },
})
export class ToastsContainer {
  toastService = inject(ToastService);

  trackByFn(index: number, toast: any): number {
    return toast.id; // Assuming each toast has a unique 'id' property
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../services/employee.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
})
export class EmployeeDetailComponent implements OnInit {
  employeeId?: number;
  employee: any = {
    id: null,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: null,
    status: '',
    group: '',
    description: '',
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.employeeId = +params['id'];
      this.employee = this.employeeService.getEmployeeById(this.employeeId);
    });
  }

  onSubmit(): void {
    this.router.navigate(['/employee']);
  }
  formatDate(dateString: string): string {
    if (!dateString) return '';

    const dateObject = new Date(dateString);

    // Check if the dateObject is valid
    if (isNaN(dateObject.getTime())) {
      // Invalid date
      return '';
    }

    const year = dateObject.getFullYear().toString();
    let month = (dateObject.getMonth() + 1).toString();
    let day = dateObject.getDate().toString();

    // Pad month and day with leading zeros if needed
    if (month.length === 1) {
      month = '0' + month;
    }
    if (day.length === 1) {
      day = '0' + day;
    }

    const formattedDate = `${year}-${month}-${day}`;

    return formattedDate;
  }
  clickBack(): void {
    this.authService.navigateBack();
  }
}

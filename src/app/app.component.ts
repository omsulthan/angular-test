import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'frontend-test';
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    switch (!this?.auth?.isLoggedIn()) {
      case true:
        this?.router?.navigate(['/login']);
        break;
      case false:
        this?.router?.navigate(['/employee']);
        break;
    }
  }

  getUsername(): void {
    return this.auth.getUser();
  }

  onLogout(): void {
    this.auth.logout();
  }
  onLogin(): boolean {
    return this.auth.isLoggedIn();
  }
}

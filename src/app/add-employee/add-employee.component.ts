import { Component, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss'],
})
export class AddEmployeeComponent {
  today: string;
  searchText: string = '';
  showDropdown: boolean = false;

  @ViewChild('searchGroupInput')
  searchGroupInput!: ElementRef<HTMLInputElement>;

  constructor(
    private routerService: Router,
    private authService : AuthService
  ) {
    this.today = new Date().toISOString().split('T')[0];
  }

  employee: any = {
    id: null,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: '',
    basicSalary: null,
    status: 'Active',
    group: [], // Change to array to match multiple selection
    description: '',
  };

  filteredGroups: string[] = [];

  groups: string[] = [
    'Group 1',
    'Group 2',
    'Group 3',
    'Group 4',
    'Group 5',
    'Group 6',
    'Group 7',
    'Group 8',
    'Group 9',
    'Group 10',
  ];

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.filteredGroups = this.groups;
  }

  filterGroups(): void {
    this.filteredGroups = this.groups.filter((group) =>
      group.toLowerCase().includes(this.searchText.toLowerCase())
    );
  }

  onGroupSelectionChange(event: any): void {
    const result = event.target.value.match(/'([^']+)'/)[1];
    this.showDropdown = false;
    this.searchText = result;
    this.employee.group = result;
    this.filterGroups();
  }

  ngAfterViewInit(): void {
    if (this.searchGroupInput) {
      console.log(this.searchGroupInput.nativeElement.offsetWidth);
    }
  }

  onSubmit(form: NgForm): void {
    if (form.valid) {
      console.log('Form submitted!');
      console.log('Employee data:', this.employee);
      this.routerService.navigate(['/employee']);
    } else {
      console.log('Form is invalid. Please check the fields.');
    }
  }
  clickBack(): void {
    this.authService.navigateBack();
  }
}
